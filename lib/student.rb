
#
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.
#
class Student
  #student class is unfinished, but the course class works fine standalone

  attr_accessor :first_name, :last_name, :name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @name = "#{@first_name} #{@last_name}"
    @courses = []
  end

  def enroll(course)
    #!!!! raise error # this is enough
    raise 'error' if self.has_conflict?(course)
    @courses << course
    @courses.uniq!

    course.students << self
    course.students.uniq!
  end

  def has_conflict?(course)
    period = course.period
    days = course.days
    self.courses.each do |course|
      return true if course.period == period && (course.days & days).any?
    end
    false
  end

  def course_load
    @course_load = Hash.new(0)

    @courses.each do |course_object|
      @course_load[course_object.department] += course_object.credits
    end
    @course_load
  end

end
